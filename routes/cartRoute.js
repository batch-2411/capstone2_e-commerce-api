const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController');
const auth = require('../utils/auth');

// Route for Get Cart
router.get('/', auth.verify, cartController.retrieveCart);

// Route for Add to Cart
router.post('/add-product', auth.verify, cartController.addProductToCart);

// Route for Changing Product Quantities in Cart
router.patch('/change-quantity', auth.verify, cartController.changeProductQuantity);

// Route for Removing a Product
router.delete('/remove-product', auth.verify, cartController.removeProduct);
module.exports = router;