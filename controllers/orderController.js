const Order = require('../models/Order');
const Cart = require('../models/Cart');
const User = require('../models/User');
const Product = require('../models/Product');
const auth = require('../utils/auth');

// Create Order
module.exports.createOrder = async (req, res) => {
	// // Get isAdmin value
	// let isAdmin = auth.decode(req.headers.authorization).isAdmin;

	// // If admin send not authorized
	// if(isAdmin) return res.status(401).json("You are not allowed to order");

	let userId = auth.decode(req.headers.authorization).id;
	try {
		let user = await User.findById(userId);
		let subTotal = await Promise.all(req.body.products.map(async product => {
			try {
				let item = await Product.findById(product.productId);
				return item.price * product.quantity;
			} catch(err) {
				res.status(400).json(err.message);
			}
		}));
		let newOrder = new Order({
		userId: userId,
		userEmail: user.email,
		...req.body,
		totalAmount: subTotal.reduce((a,b) => a + b)
		});
		let savedOrder = await newOrder.save();
		let deletedCart = await Cart.deleteOne({userId: userId, isActive:true});
		res.status(201).json(savedOrder);
	} catch(err) {
		res.status(400).json(err.message);
	}
}

// Retrieve Authenticated User Order
module.exports.retieveUserOrders = async (req, res) => {
	let userId = auth.decode(req.headers.authorization).id;

	try {
		let orders = await Order.find({userId: userId});
		res.status(200).json(orders);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Retrieve All Orders
module.exports.retrieveAllOrders = async (req, res) => {
	// Get isAdmin value
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	// If not admin send not authorized
	if(!isAdmin) return res.status(401).json("You are not authorized");
	try {
		let orders = await Order.find({});
		res.status(200).json(orders);
	} catch(err) {
		res.status(400).json(err.message);
	}
}