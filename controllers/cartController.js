const Cart = require('../models/Cart');
const Product = require('../models/Product');
const auth = require('../utils/auth');

// Retrieve Active Cart
module.exports.retrieveCart = async (req, res) => {
	let userId = auth.decode(req.headers.authorization).id;

	try {
		let cart = await Cart.findOne({userId: userId, isActive:true});
		console.log(cart);
		res.status(200).json(cart);
	} catch(err) {
		res.status(400).json(err.message);
	}
}

// Add Product to Cart
module.exports.addProductToCart = async (req, res) => {
	// Get user ID
	let userId = auth.decode(req.headers.authorization).id;

	try {
		// check if an active cart exist
		let cart = await Cart.findOne({userId: userId});
		console.log(!cart);
		// let product = await Promise.all(req.body.products.map(async product => {
		// 	try {
		// 		let item = await Product.findById(product.productId);
		// 		let subProduct = {
		// 			productId: product.productId,
		// 			quantity: product.quantity,
		// 			subTotal: item.price * product.quantity
		// 		}
		// 		return subProduct;
		// 	} catch(err) {
		// 		res.status(400).json(err.message);
		// 	}
		// }));
		let product = await Product.findById(req.body.productId);
		console.log('product ' + product)
		if(!cart) {
			let newCart = new Cart({
				userId: userId,
				products: [
					{
						name: product.name,
						productId: req.body.productId,
						quantity: req.body.quantity,
						price: product.price,
						subTotal: product.price * req.body.quantity
					}
					],
				total: product.price * req.body.quantity  
			});
			let savedCart = await newCart.save();
			res.status(201).json(savedCart);
		} else {
			console.log('pushing')
			cart.products.push(
			{
				name: product.name,
				productId: req.body.productId,
				quantity: req.body.quantity,
				price: product.price,
				subTotal: product.price * req.body.quantity
			});
			console.log('before reduce')
			console.log(cart.products)
			console.log('before' + cart.total);
			cart.total = cart.products.map(product => product.subTotal).reduce((a, b) => a + b,0);
			console.log('after' + cart.total);
			console.log('after reduce')
			let savedCart = await cart.save();
			res.status(201).json(savedCart);
		}
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Change Product Quantity
module.exports.changeProductQuantity = async (req, res) => {
	// Get user ID
	let userId = auth.decode(req.headers.authorization).id;
	try {
		// let cart = await Cart.findOne({userId: userId});
		// console.log(cart.products.name);
		// let updatedCart = await cart.findOne(
		// {
		// 	"cart.products.name": req.body.productName
		// },
		// {
		// 	$set: {
		// 		"cart.products.$.quantity": req.body.quantity
		// 	}
		// });
		// res.status(200).json(updatedCart);
		// let updatedCart = await Cart.update(
		// 	{ userId: userId},
		// 	{ $set: {'cart.products.$[o].quantity': req.body.quantity}},
		// 	{ arrayFilters: [{'o.name': req.body.name}]}
		// 	);
		// res.status(200).json(updatedCart);
		let cart = await Cart.findOne({userId: userId});
		// console.log(cart);
		// cart = cart.map(item => {
		// 	if(item == "products") {
		// 		item.map(p => {
		// 			if(p.name == req.body.name) {
		// 				p.quantity = req.body.quantity
		// 			}
		// 			return p;
		// 		})
		// 		return item;
		// 	}
		// })
		let updatedProducts = cart.products.map(product => {
			if(product.name == req.body.productName) {
				console.log(product)
				product.quantity = req.body.quantity;
				product.subTotal = product.price * req.body.quantity;
				return product;
			}
			return product;
		});
		cart.products = updatedProducts;
		cart.total = cart.products.map(product => product.subTotal).reduce((a, b) => a + b,0);
		let updatedCart = await cart.save();
		res.status(200).json(updatedCart);
	} catch(err) {
		res.status(400).json(err.message);
	}
}; 

module.exports.removeProduct = async (req, res) => {
	// Get user ID
	let userId = auth.decode(req.headers.authorization).id;
	try {
		// let cart = await Cart.findOneAndUpdate({userId: userId,'products.name': req.body.productName},
		// 	{ $pull: {'products.$': { name: req.body.productName}}});
		let cart = await Cart.findOne({userId: userId});
		let updatedProducts = cart.products.filter(product => {
			if(product.name !== req.body.productName) {
				return product;
			};
		});
		if(updatedProducts.length === 0) {
			let deletedCart = await Cart.deleteOne({userId: userId, isActive:true});
			res.status(200).json(deletedCart);
		} else {
			cart.products = updatedProducts;
			if(cart.products.length > 1) {
				cart.total = cart.products.map(product => product.subTotal).reduce((a, b) => a + b,0);
			} else {
				cart.total = cart.products[0]?.subTotal;
			}
			console.log(cart);
			let updatedCart = await cart.save();
			res.status(200).json(cart);
		}
	} catch(err) {
		res.status(400).json(err.message);
	}
}

// module.exports.removeProduct = async (req, res) => {
// 	// Get user ID
// 	let userId = auth.decode(req.headers.authorization).id;
// 	try {
// 		// let cart = await Cart.findOneAndUpdate({userId: userId,'products.name': req.body.productName},
// 		// 	{ $pull: {'products.$': { name: req.body.productName}}});
// 		let cart = await Cart.findOne({userId: userId});
// 		let updatedProducts = cart.products.filter(product => {
// 			if(product.name !== req.body.productName) {
// 				return product;
// 			};
// 		});
// 		if(updatedProducts.length > 0) {
// 			console.log('inside first if' + updatedProducts);
// 			cart.products = updatedProducts;
// 			cart.total = cart.products.reduce((a, b) => a?.subTotal + b?.subTotal);
// 			let updatedCart = await cart.save();
// 			res.status(200).json(cart);
// 		} else {
// 			let deletedCart = await Cart.findByIdAndDelete({userId: userId, isActive: true});
// 			res.status(200).json(deletedCart);
// 		}
// 	} catch(err) {
// 		res.status(400).json(err.message);
// 	}
// }
