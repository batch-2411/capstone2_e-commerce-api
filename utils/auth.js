const jwt = require('jsonwebtoken');

const secret = "ECommerceAPI";

// Token Creation
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
};

// Token Verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(!token) return res.status(401).json("You are not authenticated");
	token = token.slice(7, token.length);
	jwt.verify(token, secret, (error, data) => {
		if(error) return res.status(401).json("Token is not valid");
		next();
	})
};

// Token Decode
module.exports.decode = (token) => {
	if(!token) return res.status(401).json("You are not authenticated");
	token = token.slice(7, token.length);
	return jwt.verify(token, secret, (error, data) => {
		if(error) {
			return res.status(401).json("Token is not valid");
		} else {
			return jwt.decode(token, {complete: true}).payload;
		}
	});
};