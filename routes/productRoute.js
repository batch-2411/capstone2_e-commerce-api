const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../utils/auth');

// Route for Creating a Product(Admin Only)
router.post('/create', auth.verify, productController.createProduct);

// Retrieve all Products
router.get('/', auth.verify, productController.retrieveProducts);

// Route for Retrieving all Active Products
router.get('/active', auth.verify, productController.retrieveActiveProducts);

// Route for Retrieving all Featured Products
router.get('/featured', auth.verify, productController.retrieveFeaturedProducts);

// Route for Retrieving specific Product
router.get('/:productId', productController.retrieveProduct);

// Route for Updating Product Information
router.put('/:productId', auth.verify, productController.updateProduct);

// Route for Archiving a Product
router.patch('/:productId/archive', auth.verify, productController.archiveProduct);


module.exports = router;