const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const auth = require('../utils/auth');

// Route for Creating an Order
router.post('/', auth.verify, orderController.createOrder);

// Route for Retrieving Authenticated User's Orders
router.get('/user-orders', auth.verify, orderController.retieveUserOrders);

// Route for Retrieving All orders(Admin Only)
router.get('/all-orders', auth.verify, orderController.retrieveAllOrders);

module.exports = router;