const User = require('../models/User');
const bcrypt = require('bcrypt');

const auth = require('../utils/auth');

// Register a User
module.exports.registerUser = async (req, res) => {
	try {
		// Check if the user already exist
		let user = await User.findOne({email: req.body.email});
		if(user !== null) return res.status(400).json("Email already in use")
		if(req.body.password) {
			req.body.password = bcrypt.hashSync(req.body.password, 10) 
		}
		let newUser = new User({
			...req.body
		});
		try {
			let savedUser = await newUser.save();
			res.status(201).send(savedUser); 
		} catch(err) {
			res.status(400).json(err.message);
		}
	} catch(err) {
		res.status(400).json(err.message);
	}
	
};

// Login User
module.exports.loginUser = async (req, res) => {
	// Check if the user input their email & password
	if(!req.body.email || !req.body.password) {
		res.status(400).json("Please enter your email and password");
	} else {
		try {
			let user = await User.findOne({email: req.body.email});
			if(user == null) throw new Error("Invalid credentials")
			let isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);
			if(isPasswordCorrect) {
				res.status(200).json({message: "Successfully logged in!", access: auth.createAccessToken(user)});
			} else {
				throw new Error("Incorrect Password");
			}
		} catch(err) {
			res.status(401).json(err.message);
		}
	}
};

