const Product = require('../models/Product');
const auth = require('../utils/auth');

// Create Product
module.exports.createProduct = async (req, res) => {
	// Get isAdmin value
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;

	// If not admin send not authorized
	if(!isAdmin) return res.status(401).json("You are not authorized");
	let newProduct = new Product({
		...req.body
	});
	try {
		let savedProduct = await newProduct.save();
		res.status(201).json(savedProduct);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Retrieve All Products
module.exports.retrieveProducts = async (req, res) => {
	// Get isAdmin value
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;

	// If not admin send not authorized
	if(!isAdmin) return res.status(401).json("You are not authorized");
	try {
		let products = await Product.find({});
		res.status(200).json(products);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Retrieve All Active Products
module.exports.retrieveActiveProducts = async (req, res) => {
	try {
		let activeProducts = await Product.find({isActive: true});
		res.status(200).json(activeProducts);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Retrieve All Featured Products
module.exports.retrieveFeaturedProducts = async (req, res) => {
	try {
		let activeProducts = await Product.find({isFeatured: true, isActive:true});
		res.status(200).json(activeProducts);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Retrieve A specific Product
module.exports.retrieveProduct = async (req, res) => {
	let productId = req.params.productId;
	try {
		let product = await Product.findById(productId);
		res.status(200).json(product);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Update Product Information
module.exports.updateProduct = async (req, res) => {
	// Get isAdmin value
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	
	// If not admin send not authorized
	if(!isAdmin) return res.status(401).json("You are not authorized");

	let productId = req.params.productId;
	try {
		let updatedProduct = await Product.findByIdAndUpdate(productId,
		{ $set: req.body },
		{ new: true });
		res.status(200).json(updatedProduct);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Archive a Product
module.exports.archiveProduct = async (req, res) => {
	// Get isAdmin value
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;

	// If not admin send not authorized
	if(!isAdmin) return res.status(401).json("You are not authorized");

	let productId = req.params.productId;

	try {
		let archivedProduct = await Product.findByIdAndUpdate(productId,
		 { $set: req.body },
		 { new: true });
		res.status(200).json(archivedProduct);
	} catch(err) {
		res.status(400).json(err.message);
	}
}