const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const port = 4000;

// Routes
const authRoute = require('./routes/authRoute');
const productRoute = require('./routes/productRoute');
const userRoute = require('./routes/userRoute');
const orderRoute = require('./routes/orderRoute');
const cartRoute = require('./routes/cartRoute');


const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));



mongoose.set('strictQuery', false);
mongoose.connect('mongodb+srv://admin123:admin098@cluster0.khqxuap.mongodb.net/capstone_2?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.log.bind(console, 'Connection Error \n'));
db.once('open', () => console.log('Connected to MongoDB Atlas!'));

app.use('/auth', authRoute);
app.use('/products', productRoute);
app.use('/users', userRoute);
app.use('/orders', orderRoute);
app.use('/carts', cartRoute);


app.listen(port, () => console.log(`API is now online on port ${port}`));










