const User = require('../models/User');
const auth = require('../utils/auth');

const bcrypt = require('bcrypt');

// Retrieve User Details
module.exports.retrieveProfile = async (req, res) => {
	let userId = auth.decode(req.headers.authorization).id;
	try {
		let user = await User.findById(userId);
		res.status(200).json(user);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Retrieve All Users
module.exports.retrieveAllUsers = async (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(!isAdmin) return res.status(401).json("You are not authorized");

	try {
		let users = await User.find({});
		res.status(200).json(users);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Update User Details
module.exports.updateUserDetails = async (req, res) => {
	let userId = auth.decode(req.headers.authorization).id;
	if(req.body.password) {
			req.body.password = bcrypt.hashSync(req.body.password, 10) 
	}
	try {
		let user = await User.findByIdAndUpdate(userId,
		{ $set: req.body },
		{ new: true });
		res.status(200).json(user);
	} catch(err) {
		res.status(400).json(err.message);
	}
};

// Set User as Admin
module.exports.setUserAsAdmin = async (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;

	if(!isAdmin) return res.status(401).json("You are not authorized");

	let userId = req.params.userId;
	try {
		let userAsAdmin = await User.findByIdAndUpdate(userId, 
			{ $set: req.body },
			{ new: true});
		res.status(200).json(userAsAdmin);
	} catch(err) {
		res.status(400).json(err.message);
	}

}