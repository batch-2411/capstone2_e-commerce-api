const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Product name is required']
	},
	description: {
		type: String,
		required: [true, 'Description is required']
	},
	image: {
		type: String,
		default:'placeholder.png'
	},
	brand: {
		type: String,
		default: ''
	},
	// category: {
	// 	type: mongoose.Schema.Types.ObjectId,
	// 	ref: 'Category',
	// 	required: true
	// },
	price: {
		type: Number,
		default: 0
	},
	countInStock: {
		type: Number,
		required: [true, 'Number of stocks is required'],
		min: 0,
		max: 255
	},
	isActive: {
		type: Boolean,
		default: true
	},
	isFeatured: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
}, { timestamps: true }
);

module.exports = mongoose.model('Product', productSchema);