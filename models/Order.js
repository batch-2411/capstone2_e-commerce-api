const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User ID is required']
	},
	userEmail: {
		type: String,
		required: [true, 'User email is required']
	},
	products:[
	{
		productId: {
			type: String,
			required: true
		},
		name: {
			type: String,
			required: true
		},
		quantity: {
			type: Number,
			required: true,
			min: [1, 'Quantity can not be less than 1.']
		}
	}
	],
	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
}, { timestamps: true }
);

module.exports = mongoose.model('Order', orderSchema);