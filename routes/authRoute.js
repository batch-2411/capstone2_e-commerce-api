const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');


// Route for User Registration
router.post('/register', authController.registerUser);

// Route for User Login/Authentication
router.post('/login', authController.loginUser);

module.exports = router;