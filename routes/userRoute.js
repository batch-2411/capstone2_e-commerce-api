const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../utils/auth');

// Route for Retrieving User Details
router.get('/details', auth.verify, userController.retrieveProfile);

// Route for Updating User
router.get('/', auth.verify, userController.retrieveAllUsers);

// Route for Updating User
router.put('/', auth.verify, userController.updateUserDetails);

// Route for Setting a User as Admin(Admin Only)
router.patch('/:userId/set-admin', auth.verify, userController.setUserAsAdmin);

module.exports = router;